import asyncio
# 웹 소켓 모듈을 선언한다.
import websockets

async def connect():
    # 웹 소켓에 접속을 합니다.
    async with websockets.connect("ws://localhost:9998") as websocket:
        while True:
            send_msg = input("전송할 메세지를 입력하세요.")
            if not send_msg:
                print(dir(websocket))
                break
            await websocket.send(send_msg)
            # 웹 소켓 서버로 부터 메시지가 오면 콘솔에 출력합니다.
            data = await websocket.recv()
            print(data)
# 비동기로 서버에 접속한다.
asyncio.get_event_loop().run_until_complete(connect())
