'''float L1 = 12;
float L2 = 13.5;
float offset_z = 10;

int degree[3];

void get_angle_from_xyz(float x, float y, float z){
 
  double def_x = x;
  double def_y = y;
  double def_z = z - offset_z;

  double c = sqrt(pow(def_x,2)+pow(def_y,2));

  degree[0] = -1*(atan2 (def_y, def_x)* 57296 / 1000);
  degree[1] = 90-(atan2(def_z,c)+acos((pow(c,2)+pow(def_z,2)+pow(L1,2)-pow(L2,2))/(2*L1*sqrt(pow(c,2)+pow(def_z,2)))) )* 57296 / 1000;
  degree[2] = -1*(acos ((pow(c,2)+pow(z,2)-pow(L1,2)-pow(L2,2))/(2*L1*L2))* 57296 / 1000);
 
}'''
import math

L1 = 12
L2 = 13.5
OFFSET_X = 7
OFFSET_Z = -10

def get_angle_from_xyz(xyz):
    x, y, z = xyz[0]+OFFSET_X, xyz[1], xyz[2]+OFFSET_Z
    c = math.sqrt(x*x+y*y)
    return str(round(-(math.atan2(y, x)*57296/1000))),\
        str(round(90-(math.atan2(z,c)+math.acos((c*c+z*z+L1*L1-L2*L2)/(2*L1*math.sqrt(c*c+z*z))))*57296/1000)),\
            str(round(-(math.acos((c*c+z*z-L1*L1-L2*L2)/(2*L1*L2)))*57296/1000))


#f = open('xyz.txt', 'r')

#xyzs = [list(map(float,i.split(','))) for i in f.readlines()]

#print(xyzs)
#print(list(map(get_angle_from_xyz, xyzs)))