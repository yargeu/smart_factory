import asyncio
from typing import Text
# 웹 소켓 모듈을 선언한다.
import websockets, json, serial, time

from coordi_angle import get_angle_from_xyz

ser = serial.Serial(
    port='COM4',
    baudrate=9600
)

# 클라이언트 접속이 되면 호출된다.
async def accept(websocket, path):
    while True:
        # 클라이언트로부터 메시지를 대기한다.
        data = await websocket.recv()
        print(data)
        xyzs = [s.split(',') for s in data.split('\n')]
        angles = [get_angle_from_xyz(xyz) for xyz in xyzs]
        send_ar = ';'.join(map(lambda s:','.join(s), angles))
        print(send_ar)
        ser.write(send_ar.encode())
        time.sleep(0.5)
        recv_str = ser.read_all()
        print(recv_str)
        # 클라인언트로 echo를 붙여서 재 전송한다.
        await websocket.send("echo : " + json.dumps(angles))
# 웹 소켓 서버 생성.호스트는 localhost에 port는 9998로 생성한다.
start_server = websockets.serve(accept, "0.0.0.0", 9998)
# 비동기로 서버를 대기한다.
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()

